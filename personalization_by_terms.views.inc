<?php

/**
 * @file
 * Provide a custom views field data that isn't tied to any other module.
 */

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_data_alter().
 */
function personalization_by_terms_views_data_alter(&$data) {

  $data['node']['personalization_by_terms'] = [
    'group' => t('Personalization by terms'),
    'title' => t('Similarity from visitor'),
    'help' => t('Percentage/count of terms which node has in common with given terms.'),
    'field' => [
      'id' => 'personalization_by_terms_terms_field',
    ],
    'sort' => [
      'id' => 'personalization_by_terms_terms_field_terms_sort',
    ],
  ];
}

