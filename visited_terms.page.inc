<?php

/**
 * @file
 * Contains visited_terms.page.inc.
 *
 * Page callback for Visited terms entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Visited terms templates.
 *
 * Default template: visited_terms.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_visited_terms(array &$variables) {
  // Fetch VisitedTerms Entity Object.
  $visited_terms = $variables['elements']['#visited_terms'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
