<?php

namespace Drupal\personalization_by_terms\Plugin\views\sort;

use Drupal\personalization_by_terms\ViewsQueryTrait;
use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Handler which sort by the similarity.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("personalization_by_terms_terms_field_terms_sort")
 */
class VisitorTermsSort extends SortPluginBase {

  use ViewsQueryTrait;

  /**
   * Define default sorting order.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['order'] = ['default' => 'DESC'];
    return $options;
  }

  /**
   * Add orderBy.
   */
  public function query() {
    $this->ensureMyTable();
    $sub_query = $this->getSubQuery();
    if (empty($sub_query)) {
      return;
    }
    $this->query->addOrderBy(NULL, $sub_query, $this->options['order'], 'subquery');
  }

}
