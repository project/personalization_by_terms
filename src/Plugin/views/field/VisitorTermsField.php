<?php

namespace Drupal\personalization_by_terms\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\personalization_by_terms\ViewsQueryTrait;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Shows the similarity of the node.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("personalization_by_terms_terms_field")
 */
class VisitorTermsField extends FieldPluginBase {

  use ViewsQueryTrait;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $sub_query = $this->getSubQuery();
    if (empty($sub_query)) {
      return;
    }
    $this->field_alias = $this->query->addField(NULL, $sub_query, 'termcount');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['count_type'] = ['default' => 1];
    $options['percent_suffix'] = ['default' => 1];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['count_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display type'),
      '#default_value' => $this->options['count_type'],
      '#options' => [
        0 => $this->t('Show count of common terms'),
        1 => $this->t('Show as percentage'),
      ],
    ];

    $form['percent_suffix'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Append % when showing percentage'),
      '#default_value' => !empty($this->options['percent_suffix']),
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($this->options['count_type'] == 0) {
      return $values->{$this->field_alias};
    }
    elseif ($this->getTids()) {
      $output = round($values->{$this->field_alias} / count($this->getTids()) * 100);
      if (!empty($this->options['percent_suffix'])) {
        $output .= '%';
      }
      return $output;
    }
  }

}
