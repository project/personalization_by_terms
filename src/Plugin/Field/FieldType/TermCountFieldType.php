<?php

namespace Drupal\personalization_by_terms\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'term_count' field type.
 *
 * @FieldType(
 *   id = "term_count",
 *   label = @Translation("Term count"),
 *   description = @Translation("Count of taxonomy terms"),
 *   default_widget = "term_count_widget_type",
 *   default_formatter = "term_count_formatter_type"
 * )
 */
class TermCountFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['tid'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Term ID'))
      ->setRequired(FALSE);

    $properties['count'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Count'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'tid' => [
          'type' => 'int',
          'size' => 'big',
          'unsigned' => TRUE,
        ],
        'count' => [
          'type' => 'int',
          'size' => 'big',
          'unsigned' => TRUE,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = [
      'tid' => 1,
      'count' => 1,
    ];
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('tid')->getValue();
    return empty($value);
  }

}
