<?php

namespace Drupal\personalization_by_terms\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Plugin implementation of the 'donation_amount_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "term_count_widget_type",
 *   module = "personalization_by_terms",
 *   label = @Translation("Term count widget"),
 *   field_types = {
 *     "term_count"
 *   }
 * )
 */
class TermCountWidgetType extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $tid = $items[$delta]->tid;
    $term = !empty($tid) ? Term::load($tid) : NULL;
    $element['tid'] = [
      '#title' => $this->t('Taxonomy term'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#default_value' => $term,
      '#prefix' => '<table class="term-count-wrapper"></tr><td>',
      '#suffix' => '</td>',
    ];
    $element['count'] = [
      '#title' => $this->t('Count'),
      '#type' => 'number',
      '#default_value' => isset($items[$delta]->count) ? $items[$delta]->count : '',
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $element;
  }

}
