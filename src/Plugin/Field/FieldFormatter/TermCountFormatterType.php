<?php

namespace Drupal\personalization_by_terms\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Plugin implementation of the 'donation_amount_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "term_count_formatter_type",
 *   label = @Translation("Term count formatter"),
 *   field_types = {
 *     "term_count"
 *   }
 * )
 */
class TermCountFormatterType extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'container',
      ];
      $tid = $item->getValue()['tid'];
      $term = Term::load($tid);
      $term_value = empty($term) ? '<Not available>' : $term->label() . "($tid)";
      $elements[$delta]['tid'] = [
        '#markup' => $this->viewValue($term_value),
      ];
      $elements[$delta]['count'] = [
        '#markup' => $this->viewValue($item->getValue()['count']),
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue($value) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($value));
  }

}
