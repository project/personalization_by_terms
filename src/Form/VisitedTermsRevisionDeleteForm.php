<?php

namespace Drupal\personalization_by_terms\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Visited terms revision.
 *
 * @ingroup personalization_by_terms
 */
class VisitedTermsRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Visited terms revision.
   *
   * @var \Drupal\personalization_by_terms\Entity\VisitedTermsInterface
   */
  protected $revision;

  /**
   * The Visited terms storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $visitedTermsStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->visitedTermsStorage = $container->get('entity_type.manager')->getStorage('visited_terms');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'visited_terms_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.visited_terms.version_history', ['visited_terms' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $visited_terms_revision = NULL) {
    $this->revision = $this->VisitedTermsStorage->loadRevision($visited_terms_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->VisitedTermsStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Visited terms: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Visited terms %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.visited_terms.canonical',
       ['visited_terms' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {visited_terms_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.visited_terms.version_history',
         ['visited_terms' => $this->revision->id()]
      );
    }
  }

}
