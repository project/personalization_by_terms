<?php

namespace Drupal\personalization_by_terms\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Visited terms entities.
 *
 * @ingroup personalization_by_terms
 */
class VisitedTermsDeleteForm extends ContentEntityDeleteForm {


}
