<?php

namespace Drupal\personalization_by_terms\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Class VisitedTermsTypeForm.
 */
class VisitedTermsTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsTypeInterface $visited_terms_type */
    $visited_terms_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $visited_terms_type->label(),
      '#description' => $this->t("Label for the Visited terms type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $visited_terms_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\personalization_by_terms\Entity\VisitedTermsType::load',
      ],
      '#disabled' => !$visited_terms_type->isNew(),
    ];

    $vocabulary_entities = Vocabulary::loadMultiple();
    foreach($vocabulary_entities as $vocabulary) {
      $vocabularies[$vocabulary->id()] = $vocabulary->label();
    }

    $form['vocabularies'] = [
      '#type' => 'checkboxes',
      '#options' => $vocabularies,
      '#title' => $this->t('Vocabularies'),
      '#description' => $this->t('Select the vocabularies to track.'),
      '#default_value' => explode(',', $visited_terms_type->getVocabularies()),
      '#required' => TRUE,
    ];

    $bundles_types = NodeType::loadMultiple();
    foreach($bundles_types as $bundle) {
      $bundles[$bundle->id()] = $bundle->label();
    }
    $form['content_bundles'] = [
      '#type' => 'checkboxes',
      '#options' => $bundles,
      '#title' => $this->t('Content Bundles'),
      '#description' => $this->t('Select the content bundles to track.'),
      '#default_value' => explode(',', $visited_terms_type->getContentBundles()),
      '#required' => TRUE,
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  public function buildEntity(array $form, FormStateInterface $form_state) {
    $keys = [
      'vocabularies',
      'content_bundles',
    ];
    foreach($keys as $key) {
      $this->checkboxesValueToString($key, $form_state);
    }
    return parent::buildEntity($form, $form_state);
  }

  /**
   * Convert checkbox values to string, for DB schema saving
   *
   * @param $key
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  protected function checkboxesValueToString($key, FormStateInterface &$form_state) {
    $values = $form_state->getValue($key);
    if (is_array($values)) {
      foreach($values as $value) {
        if (!empty($value)) {
          $save_value[] = $value;
        }
      }
      $save_value = implode(',', $save_value);
      $form_state->setValue($key, $save_value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $visited_terms_type = $this->entity;
    $status = $visited_terms_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Visited terms type.', [
          '%label' => $visited_terms_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Visited terms type.', [
          '%label' => $visited_terms_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($visited_terms_type->toUrl('collection'));
  }

}
