<?php

namespace Drupal\personalization_by_terms;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\personalization_by_terms\Entity\VisitedTermsInterface;

/**
 * Defines the storage handler class for Visited terms entities.
 *
 * This extends the base storage class, adding required special handling for
 * Visited terms entities.
 *
 * @ingroup personalization_by_terms
 */
class VisitedTermsStorage extends SqlContentEntityStorage implements VisitedTermsStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(VisitedTermsInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {visited_terms_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {visited_terms_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
