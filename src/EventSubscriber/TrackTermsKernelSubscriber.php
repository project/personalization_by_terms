<?PHP

/**
 * @file
 */

namespace Drupal\personalization_by_terms\EventSubscriber;

use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\FinishResponseSubscriber;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\personalization_by_terms\Entity\VisitedTermsType;
use Drupal\personalization_by_terms\TermsTrackingServiceInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Tracks terms and disable cache
 */
class TrackTermsKernelSubscriber extends FinishResponseSubscriber implements EventSubscriberInterface {

  /** @var TermsTrackingServiceInterface */
  protected $terms_tracking_service;

  public function __construct(TermsTrackingServiceInterface $termsTrackingService, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, RequestPolicyInterface $request_policy, ResponsePolicyInterface $response_policy, CacheContextsManager $cache_contexts_manager, $http_response_debug_cacheability_headers = FALSE) {
    $this->terms_tracking_service = $termsTrackingService;
    parent::__construct($language_manager, $config_factory, $request_policy, $response_policy, $cache_contexts_manager, $http_response_debug_cacheability_headers);
  }

  /**
   * Sets extra headers on successful responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    if (!$this->terms_tracking_service->trackNode()) {
      return;
    }

    $request = $event->getRequest();
    $response = $event->getResponse();

    $this->setResponseNotCacheable($response, $request);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }
}
