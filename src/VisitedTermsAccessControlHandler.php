<?php

namespace Drupal\personalization_by_terms;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Visited terms entity.
 *
 * @see \Drupal\personalization_by_terms\Entity\VisitedTerms.
 */
class VisitedTermsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished visited terms entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published visited terms entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit visited terms entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete visited terms entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add visited terms entities');
  }


}
