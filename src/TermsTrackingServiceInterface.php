<?php

namespace Drupal\personalization_by_terms;

use Drupal\node\NodeInterface;

interface TermsTrackingServiceInterface {

  /**
   * Get a list of all tracked bundles
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTrackedBundles();

  /**
   * Get a list of all tracked bundles
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTrackedBundlesWithVocabularies();

  /**
   * @return bool
   */
  public function shouldTrack();

  /**
   * @param \Drupal\node\NodeInterface|null $node
   *
   * @return bool
   */
  public function trackNode(NodeInterface $node = NULL);

  /**
   * @param $visitedTermsType
   * @param $tids
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveTracking($visitedTermsType, $tids);

  /**
   * Returns an array in the form of [Term1::id => count of term 1 views, etc.]
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getVisitorTerms();

}
