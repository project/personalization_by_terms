<?php

namespace Drupal\personalization_by_terms;

use Drupal\taxonomy\Entity\Term;

trait ViewsQueryTrait {

  protected $tids;

  /**
   * @return array|mixed|null
   */
  protected function getTids() {
    $tids = &drupal_static(__FUNCTION__);
    if (!$tids) {
      $termsTrackingService = \Drupal::service('personalization_by_terms.terms_tracking_service');
      $term_counts = $termsTrackingService->getVisitorTerms();

      $this->tids = [];
      foreach ($term_counts as $tid => $count) {
        $this->tids[$tid] = $tid;
        $term = Term::load($tid);
        if (!empty($term)) {
          $this->terms[$tid] = $term->label();
        }
      }
      $tids = $this->tids;
    }
    return $tids;
  }

  /**
   * @return string
   */
  protected function getSubQuery() {
    $sub_query = &drupal_static(__FUNCTION__);
    if (!$sub_query) {
      $tids = $this->getTids();
      if (empty($tids)) {
        return FALSE;
      }
      $sub_query = "(IFNULL((SELECT COUNT(ti.tid) "
        . "FROM taxonomy_index ti "
        . "LEFT JOIN node n ON n.nid = ti.nid "
        . "WHERE tid IN (" . implode(',', $tids) . ") "
        . "AND n.nid = node_field_data.nid),'0'))";
    }
    return $sub_query;
  }
}
