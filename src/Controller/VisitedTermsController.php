<?php

namespace Drupal\personalization_by_terms\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\personalization_by_terms\Entity\VisitedTermsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VisitedTermsController.
 *
 *  Returns responses for Visited terms routes.
 */
class VisitedTermsController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Visited terms revision.
   *
   * @param int $visited_terms_revision
   *   The Visited terms revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($visited_terms_revision) {
    $visited_terms = $this->entityTypeManager()->getStorage('visited_terms')
      ->loadRevision($visited_terms_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('visited_terms');

    return $view_builder->view($visited_terms);
  }

  /**
   * Page title callback for a Visited terms revision.
   *
   * @param int $visited_terms_revision
   *   The Visited terms revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($visited_terms_revision) {
    $visited_terms = $this->entityTypeManager()->getStorage('visited_terms')
      ->loadRevision($visited_terms_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $visited_terms->label(),
      '%date' => $this->dateFormatter->format($visited_terms->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Visited terms.
   *
   * @param \Drupal\personalization_by_terms\Entity\VisitedTermsInterface $visited_terms
   *   A Visited terms object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(VisitedTermsInterface $visited_terms) {
    $account = $this->currentUser();
    $visited_terms_storage = $this->entityTypeManager()->getStorage('visited_terms');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $visited_terms->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all visited terms revisions") || $account->hasPermission('administer visited terms entities')));
    $delete_permission = (($account->hasPermission("delete all visited terms revisions") || $account->hasPermission('administer visited terms entities')));

    $rows = [];

    $vids = $visited_terms_storage->revisionIds($visited_terms);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\personalization_by_terms\VisitedTermsInterface $revision */
      $revision = $visited_terms_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $visited_terms->getRevisionId()) {
          $link = $this->l($date, new Url('entity.visited_terms.revision', [
            'visited_terms' => $visited_terms->id(),
            'visited_terms_revision' => $vid,
          ]));
        }
        else {
          $link = $visited_terms->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.visited_terms.revision_revert', [
                'visited_terms' => $visited_terms->id(),
                'visited_terms_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.visited_terms.revision_delete', [
                'visited_terms' => $visited_terms->id(),
                'visited_terms_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['visited_terms_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
