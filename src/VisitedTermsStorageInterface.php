<?php

namespace Drupal\personalization_by_terms;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\personalization_by_terms\Entity\VisitedTermsInterface;

/**
 * Defines the storage handler class for Visited terms entities.
 *
 * This extends the base storage class, adding required special handling for
 * Visited terms entities.
 *
 * @ingroup personalization_by_terms
 */
interface VisitedTermsStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Visited terms revision IDs for a specific Visited terms.
   *
   * @param \Drupal\personalization_by_terms\Entity\VisitedTermsInterface $entity
   *   The Visited terms entity.
   *
   * @return int[]
   *   Visited terms revision IDs (in ascending order).
   */
  public function revisionIds(VisitedTermsInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Visited terms author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Visited terms revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
