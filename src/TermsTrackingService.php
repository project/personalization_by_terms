<?php

namespace Drupal\personalization_by_terms;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\personalization_by_terms\Entity\VisitedTerms;
use Drupal\personalization_by_terms\Entity\VisitedTermsInterface;

class TermsTrackingService implements TermsTrackingServiceInterface {

  /** @var SessionManagerInterface */
  protected $session_manager;

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
  protected $entity_type_manager;

  /** @var \Drupal\node\NodeInterface */
  protected $node;

  /**
   * @param \Drupal\Core\Session\SessionManagerInterface $sessionManager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(SessionManagerInterface $sessionManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->session_manager = $sessionManager;
    $this->entity_type_manager = $entityTypeManager;

    if (\Drupal::routeMatch()->getRouteName() != 'entity.node.canonical') {
      $this->node = NULL;
      return;
    }
    $this->node = \Drupal::routeMatch()->getParameter('node');
  }

  /**
   * @inheritDoc
   */
  public function shouldTrack() {
    try {
      $content_types = $this->getTrackedBundles();
      return in_array($this->node->bundle(), $content_types);
    }
    catch (\Throwable $ex) {
      \Drupal::logger('personalization_by_terms')->error('shouldTrack - ' . $ex->getMessage());
      return FALSE;
    }
  }

  /**
   * @inheritDoc
   */
  public function getTrackedBundles() {
    // TODO: cache this
    $return = [];
    $types = \Drupal::entityTypeManager()
      ->getStorage('visited_terms_type')
      ->loadMultiple();
    /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsTypeInterface $visitedTermsType */
    foreach($types as $visitedTermsType) {
      $bundles = $visitedTermsType->getContentBundles();
      $bundles = explode(',', $bundles);
      $return = array_merge($return, $bundles);
    }
    return $return;
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTrackedBundlesWithVocabularies() {
    // TODO: cache this
    $return = [];
    $types = \Drupal::entityTypeManager()
      ->getStorage('visited_terms_type')
      ->loadMultiple();
    /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsTypeInterface $visitedTermsType */
    foreach($types as $visitedTermsType) {
      $bundles = $visitedTermsType->getContentBundles();
      $bundles = explode(',', $bundles);
      $vocabularies = $visitedTermsType->getVocabularies();
      $vocabularies = explode(',', $vocabularies);
      foreach($bundles as $bundle) {
        foreach($vocabularies as $vocabulary) {
          $return[$bundle][$vocabulary]['visitedTermsTypes'][$visitedTermsType->id()] = $visitedTermsType->id();
        }
      }
    }
    return $return;
  }

  /**
   * @inheritDoc
   */
  public function trackNode(NodeInterface $node = NULL) {
    if (empty($node)) {
      $node = $this->node;
    }
    if (empty($node) || !$this->shouldTrack()) {
      return FALSE;
    }
    $trackingRequirements = $this->getTrackedBundlesWithVocabularies();
    $trackingRequirements = $trackingRequirements[$node->bundle()];
    if (empty($trackingRequirements)) {
      return FALSE;
    }
    $definitions = $node->getFieldDefinitions();
    foreach($definitions as $key => $definition) {
      if (!$definition instanceof FieldConfigInterface) {
        continue;
      }
      $settings = $definition->get('settings');
      if (!empty($settings['handler'])
        && !empty($settings['handler_settings']['target_bundles'])
        && strpos($settings['handler'], 'taxonomy_term') !== FALSE
      ) {
        $field_vocabs = $settings['handler_settings']['target_bundles'];
        foreach($field_vocabs as $field_vocab) {
          if (!empty($trackingRequirements[$field_vocab])) {
            $field_names[$definition->getName()] = $trackingRequirements[$field_vocab]['visitedTermsTypes'];
          }
        }
      }
    }
    if (empty($field_names)) {
      return FALSE;
    }
    foreach($field_names as $field_name => $trackingTypes) {
      $terms = $node->get($field_name)->getValue();
      foreach($terms as $term) {
        foreach($trackingTypes as $trackingType) {
          $dataToStore[$trackingType][$term['target_id']] = $term['target_id'];
        }
      }
    }
    foreach($dataToStore as $trackingType => $tids) {
      $this->saveTracking($trackingType, $tids);
    }
    return TRUE;
  }

  /**
   * @inheritDoc
   */
  public function saveTracking($visitedTermsType, $tids) {
    $id = NULL;
    $visitedTermsEntity = $this->getVisitedTermEntity($visitedTermsType, $id);
    if (empty($visitedTermsEntity)) {
      $values = [
        'type' => $visitedTermsType,
        'user_id' => \Drupal::currentUser()->id(),
        'name' => $id,
        'session_id' => $this->session_manager->getId(),
      ];
      $visitedTermsEntity = VisitedTerms::create($values);
    }
    foreach($tids as $tid) {
      $visitedTermsEntity->addTermCount($tid);
    }
    $visitedTermsEntity->save();
  }

  /**
   * @inheritDoc
   */
  public function getVisitorTerms() {
    /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsInterface[] $visitedTermsEntities */
    if (\Drupal::currentUser()->isAnonymous()) {
      if (!isset($_SESSION['session_started'])) {
        return [];
      }
      $id = $this->session_manager->getName();
      $visitedTermsEntities = $this->entity_type_manager
        ->getStorage('visited_terms')
        ->loadByProperties(['session_id' => $id]);
    }
    else {
      $id = \Drupal::currentUser()->id();
      $visitedTermsEntities = $this->entity_type_manager
        ->getStorage('visited_terms')
        ->loadByProperties(['user_id' => $id]);
    }
    if (empty($visitedTermsEntities)) {
      return [];
    }

    foreach($visitedTermsEntities as $visitedTermsEntity) {
      $term_counts = $visitedTermsEntity->getTermCounts();
      $terms = [];
      foreach($term_counts as $term_count) {
        $tid = $term_count['tid'];
        $count = $term_count['count'];
        if (empty($terms[$tid])) {
          $terms[$tid] = $count;
        }
        else {
          $terms[$tid] += $count;
        }
      }
    }
    arsort($terms, SORT_NUMERIC);
    return $terms;
  }

  /**
   * @param $visitedTermsType
   *
   * @return \Drupal\personalization_by_terms\Entity\VisitedTermsInterface|false|mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getVisitedTermEntity($visitedTermsType, &$id) {
    /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsInterface[] $visitedTermsEntity */
    /** @var VisitedTermsInterface $visitedTermsEntity */
    // see if the user is anonymous - if so, use a session ID as the key
    if (\Drupal::currentUser()->isAnonymous()) {
      // start a new session if there isn't one
      if (!isset($_SESSION['session_started'])) {
        $_SESSION['session_started'] = TRUE;
        $this->session_manager->start();
      }
      // search for a matching session ID
      $id = $this->session_manager->getId();
      $visitedTermsEntities = $this->entity_type_manager
        ->getStorage('visited_terms')
        ->loadByProperties(['type' => [$visitedTermsType], 'session_id' => $id]);
      $visitedTermsEntity = reset($visitedTermsEntities);
    }
    else {
      // use the user ID instead
      $id = \Drupal::currentUser()->id();
      // search for an entity for the user
      $visitedTermsEntities = $this->entity_type_manager
        ->getStorage('visited_terms')
        ->loadByProperties(['type' => [$visitedTermsType], 'user_id' => $id]);
      $visitedTermsEntity = reset($visitedTermsEntities);
      /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsInterface[] $sessionEntities */
      $sessionEntities = $this->entity_type_manager
        ->getStorage('visited_terms')
        ->loadByProperties(['type' => [$visitedTermsType], 'session_id' => $this->session_manager->getId()]);
      // merge any other entities, e.g. from before the session login
      foreach($sessionEntities as $sessionEntity) {
        if ($sessionEntity->id() != $visitedTermsEntity->id()) {
          $this->mergeVisitorTermsEntites($visitedTermsEntity, $sessionEntity);
        }
      }
    }
    return $visitedTermsEntity;
  }

  /**
   * @param \Drupal\personalization_by_terms\Entity\VisitedTermsInterface $master
   * @param \Drupal\personalization_by_terms\Entity\VisitedTermsInterface $secondary
   * @param bool $delete_secondary
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function mergeVisitorTermsEntites(VisitedTermsInterface $master, VisitedTermsInterface $secondary, bool $delete_secondary = TRUE) {
    $tids = $secondary->get('terms')->getValue();
    foreach($tids as $tid) {
      $master->addTermCount($tid);
    }
    $master->save();
    if ($delete_secondary) {
      $secondary->delete();
    }
  }
}
