<?php

namespace Drupal\personalization_by_terms;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Visited terms entities.
 *
 * @ingroup personalization_by_terms
 */
class VisitedTermsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Visited terms ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\personalization_by_terms\Entity\VisitedTerms $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.visited_terms.edit_form',
      ['visited_terms' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
