<?php

namespace Drupal\personalization_by_terms\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Visited terms type entities.
 */
interface VisitedTermsTypeInterface extends ConfigEntityInterface {

  /**
   * @return string
   */
  public function getVocabularies();

  /**
   * @return string
   */
  public function getContentBundles();

}
