<?php

namespace Drupal\personalization_by_terms\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Visited terms type entity.
 *
 * @ConfigEntityType(
 *   id = "visited_terms_type",
 *   label = @Translation("Visited terms type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\personalization_by_terms\VisitedTermsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\personalization_by_terms\Form\VisitedTermsTypeForm",
 *       "edit" = "Drupal\personalization_by_terms\Form\VisitedTermsTypeForm",
 *       "delete" = "Drupal\personalization_by_terms\Form\VisitedTermsTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\personalization_by_terms\VisitedTermsTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "visited_terms_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "visited_terms",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "vocabularies",
 *     "content_bundles",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/visited_terms_type/{visited_terms_type}",
 *     "add-form" = "/admin/structure/visited_terms_type/add",
 *     "edit-form" = "/admin/structure/visited_terms_type/{visited_terms_type}/edit",
 *     "delete-form" = "/admin/structure/visited_terms_type/{visited_terms_type}/delete",
 *     "collection" = "/admin/structure/visited_terms_type"
 *   }
 * )
 */
class VisitedTermsType extends ConfigEntityBundleBase implements VisitedTermsTypeInterface {

  /**
   * The Visited terms type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Visited terms type label.
   *
   * @var string
   */
  protected $label;

  /**
   * Vocabularies to track
   *
   * @var string
   */
  protected $vocabularies;

  /**
   * Content bundles to track
   *
   * @var string
   */
  protected $content_bundles;

  /**
   * @inheritDoc
   */
  public function getVocabularies() {
    return $this->vocabularies;
  }

  /**
   * @inheritDoc
   */
  public function getContentBundles() {
    return $this->content_bundles;
  }

}
