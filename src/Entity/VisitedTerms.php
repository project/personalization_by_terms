<?php

namespace Drupal\personalization_by_terms\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Visited terms entity.
 *
 * @ingroup personalization_by_terms
 *
 * @ContentEntityType(
 *   id = "visited_terms",
 *   label = @Translation("Visited terms"),
 *   bundle_label = @Translation("Visited terms type"),
 *   handlers = {
 *     "storage" = "Drupal\personalization_by_terms\VisitedTermsStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\personalization_by_terms\VisitedTermsListBuilder",
 *     "views_data" = "Drupal\personalization_by_terms\Entity\VisitedTermsViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\personalization_by_terms\Form\VisitedTermsForm",
 *       "add" = "Drupal\personalization_by_terms\Form\VisitedTermsForm",
 *       "edit" = "Drupal\personalization_by_terms\Form\VisitedTermsForm",
 *       "delete" = "Drupal\personalization_by_terms\Form\VisitedTermsDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\personalization_by_terms\VisitedTermsHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\personalization_by_terms\VisitedTermsAccessControlHandler",
 *   },
 *   base_table = "visited_terms",
 *   revision_table = "visited_terms_revision",
 *   revision_data_table = "visited_terms_field_revision",
 *   translatable = FALSE,
 *   admin_permission = "administer visited terms entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/visited_terms/{visited_terms}",
 *     "add-page" = "/admin/structure/visited_terms/add",
 *     "add-form" = "/admin/structure/visited_terms/add/{visited_terms_type}",
 *     "edit-form" = "/admin/structure/visited_terms/{visited_terms}/edit",
 *     "delete-form" = "/admin/structure/visited_terms/{visited_terms}/delete",
 *     "version-history" = "/admin/structure/visited_terms/{visited_terms}/revisions",
 *     "revision" = "/admin/structure/visited_terms/{visited_terms}/revisions/{visited_terms_revision}/view",
 *     "revision_revert" = "/admin/structure/visited_terms/{visited_terms}/revisions/{visited_terms_revision}/revert",
 *     "revision_delete" = "/admin/structure/visited_terms/{visited_terms}/revisions/{visited_terms_revision}/delete",
 *     "collection" = "/admin/structure/visited_terms",
 *   },
 *   bundle_entity_type = "visited_terms_type",
 *   field_ui_base_route = "entity.visited_terms_type.edit_form"
 * )
 */
class VisitedTerms extends EditorialContentEntityBase implements VisitedTermsInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the visited_terms owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Visited terms entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Visited terms entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Visited terms is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['session_id'] = BaseFieldDefinition::create('string')
      ->setLabel('Session ID')
      ->setDescription(t('The session ID of the visitor'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['terms'] = BaseFieldDefinition::create('term_count')
      ->setLabel(t('Terms'))
      ->setDescription(t('The terms tracked from the visitor'))
      ->setRevisionable(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      //->setSetting('target_type', 'taxonomy_term')
      //->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'term_count_widget_type',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * @inheritDoc
   */
  public function getTermCounts() {
    return $this->get('terms')->getValue();
  }

  /**
   * @inheritDoc
   */
  public function addTermCount($tid, int $count = 1) {
    $added = FALSE;
    $term_counts = $this->getTermCounts();
    foreach($term_counts as &$term_count) {
      if ($term_count['tid'] == $tid) {
        $term_count['count'] += $count;
        $added = TRUE;
      }
    }
    if (!$added) {
      $term_counts[] = [
        'tid' => $tid,
        'count' => $count,
      ];
    }
    $this->set('terms', $term_counts);
    return $this;
  }

}
