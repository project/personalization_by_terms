<?php

namespace Drupal\personalization_by_terms\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Visited terms entities.
 */
class VisitedTermsViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
