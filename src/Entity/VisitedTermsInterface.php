<?php

namespace Drupal\personalization_by_terms\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Visited terms entities.
 *
 * @ingroup personalization_by_terms
 */
interface VisitedTermsInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Visited terms name.
   *
   * @return string
   *   Name of the Visited terms.
   */
  public function getName();

  /**
   * Sets the Visited terms name.
   *
   * @param string $name
   *   The Visited terms name.
   *
   * @return \Drupal\personalization_by_terms\Entity\VisitedTermsInterface
   *   The called Visited terms entity.
   */
  public function setName($name);

  /**
   * Gets the Visited terms creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Visited terms.
   */
  public function getCreatedTime();

  /**
   * Sets the Visited terms creation timestamp.
   *
   * @param int $timestamp
   *   The Visited terms creation timestamp.
   *
   * @return \Drupal\personalization_by_terms\Entity\VisitedTermsInterface
   *   The called Visited terms entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Visited terms revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Visited terms revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\personalization_by_terms\Entity\VisitedTermsInterface
   *   The called Visited terms entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Visited terms revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Visited terms revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\personalization_by_terms\Entity\VisitedTermsInterface
   *   The called Visited terms entity.
   */
  public function setRevisionUserId($uid);

  /**
   * @return array
   */
  public function getTermCounts();

  /**
   * @param int $tid
   * @param int $count
   *
   * @return $this
   */
  public function addTermCount(int $tid, int $count = 1);

}
