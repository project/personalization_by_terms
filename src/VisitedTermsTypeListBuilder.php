<?php

namespace Drupal\personalization_by_terms;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Visited terms type entities.
 */
class VisitedTermsTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Visited terms type');
    $header['id'] = $this->t('Machine name');
    $header['vocabularies'] = $this->t('Tracked vocabularies');
    $header['content_bundles'] = $this->t('Tracked content bundles');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\personalization_by_terms\Entity\VisitedTermsTypeInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['vocabularies'] = $entity->getVocabularies();
    $row['content_bundles'] = $entity->getContentBundles();
    return $row + parent::buildRow($entity);
  }

}
