Personalization by Terms is a module to personalize content based upon taxonomy terms referenced in viewed nodes.

The module tracks viewed terms from selected vocabularies and content types, building an entity containing the terms and count data.

Tracking works for authenticated users via their User ID. Tracking also works for anonymous users via a session ID. This means using this module will prevent caching of pages, as session IDs are required to track correctly, and tracking usage needs to be stored. TODO: If a user logs in, the anonymous session data is converted and/or merged to an authenticated user tracking entity.

As this in a dev release only at this stage, please test and check for yourself that it works in all caching scenarios. In fact test everything and raise an issue if you find any bugs.

To set this up, add a "Visited Terms Type", located in the Structure menu. Select the content types and taxonomy vocabularies to track. Each time a matching node is viewed, a count of any referenced terms from the selected vocabularies will be added to a "Visited Terms" entity. These entities can be viewed, edited, and reported on using Views.

There are additional views fields and a sort criteria available for node based views which can be added to push more relevant nodes to the top of any lists, based upon a match on the "Visited Terms" entity data.

Sounds simple, but please ask if you have any questions or comments.
